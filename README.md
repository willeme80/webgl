# WebGL_EscapeGame
> Projet réalisé par DORION William et TRICOT Yann élèves en DUT INFO 2ème année.

```
* Nous pouvons nous déplacer avec les touches Z,Q,S et D. 
* Nous pouvons diriger le regard avec la souris.
```


***Objectif de l'Escape Game :***
* A l'aide d'une lampe torche frontale explorez cette cathédrale plongée dans l'obscurité pour retrouver les trois sphères dissimulées en son sein.

***Remarques :***
* Le nombre de sphères restantes à trouver vous est indiqué en bas à gauche de votre écran. 